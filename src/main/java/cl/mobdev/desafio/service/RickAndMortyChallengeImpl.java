package cl.mobdev.desafio.service;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;

import cl.mobdev.desafio.interf.IRickAndMortyChallenge;
import cl.mobdev.desafio.model.ResponseDesafio;
import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;
import cl.mobdev.desafio.repo.RickAndMortyApiClient;
import feign.FeignException;

@Component
public class RickAndMortyChallengeImpl implements IRickAndMortyChallenge {
	private static final Logger log = LoggerFactory.getLogger(RickAndMortyChallengeImpl.class);
	private static final String REGEX_URL_LOCATION = "^https://rickandmortyapi.com/api/location/(\\d)";

	@Autowired
	RickAndMortyApiClient rickAndMortyApiClient;
	
	@Autowired
	private ObjectMapper om;
	
	@Override
	public RMCharacter getCharacterFromServiceById(Integer id) throws FeignException {
		RMCharacter rmCharacter = rickAndMortyApiClient.getSingleCharacter(id);
		return rmCharacter;
	}

	@Override
	public Integer getIdOriginLocationFromCharacter(RMCharacter rmCharacter) {
		Pattern pattern = Pattern.compile(REGEX_URL_LOCATION);
		Matcher matcher = pattern.matcher(rmCharacter.getOrigin().getUrl());
		
		String id = null;
		while (matcher.find()) {
			id = matcher.group(1);
		}
		return id != null ? Integer.parseInt(id) : 0;
	}

	@Override
	public RMLocation getLocationFromServiceById(Integer id) throws FeignException{
		return rickAndMortyApiClient.getSingleLocation(id);
	}

	@Override
	public ResponseDesafio mergeCharacterOriginLocation(RMCharacter rmCharacter, RMLocation rmLocation) {
		ResponseDesafio response = new ResponseDesafio();
		response.setId(rmCharacter.getId());
		response.setName(rmCharacter.getName());
		response.setStatus(rmCharacter.getStatus());
		response.setSpecies(rmCharacter.getSpecies());
		response.setType(rmCharacter.getType());
		response.setEpisodeCount(rmCharacter.getEpisode().size());
		
		ResponseDesafio.Origin origin = response.new Origin();
		origin.setName(rmLocation.getName());
		origin.setUrl(rmLocation.getUrl());
		origin.setDimension(rmLocation.getDimension());
		origin.setResidents(rmLocation.getResidents());
		
		response.setOrigin(origin);
		
		return response;
	}

	@Override
	public List<String> validateResponseWhithJsonSchema(ResponseDesafio responseDesafio) throws IOException {
		JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7);

		String schemaLocation = "classpath:mobdev-schema.json";
		URL urlSchemaLocation = ResourceUtils.getURL(schemaLocation);
		JsonNode jsonNodeSchema = om.readValue(urlSchemaLocation, JsonNode.class);
		JsonSchema schema = factory.getSchema(jsonNodeSchema);

		String responseDesafioJsonString = om.writeValueAsString(responseDesafio);
		JsonNode responseDesafioJsonNode = om.readTree(responseDesafioJsonString);
		
		Set<ValidationMessage> validationMessages = schema.validate(responseDesafioJsonNode);
		
		List<String> mensajesValidacionSchema = validationMessages.stream()
				.map(valid -> {
					return valid.getMessage();
				})
				.collect(Collectors.toList());
		
		return mensajesValidacionSchema;
	}


}
