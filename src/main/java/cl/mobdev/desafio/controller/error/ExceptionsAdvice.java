package cl.mobdev.desafio.controller.error;

import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;

import feign.FeignException;

@RestControllerAdvice
public class ExceptionsAdvice {
	
	@ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<?> handleNumberFormatException(
    		NumberFormatException numberFormatException) {
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body("¡que tal si pruebas con un número entero!");
	}
	
	@ExceptionHandler(FeignException.class)
    public ResponseEntity<?> handleFeignException(
    		FeignException feignException) {
		switch (feignException.status()) {
		case 404:
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("¡personaje no encontrado!");

		default:
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("¡no he podido conectar con api!");
		}
	}
	
	@ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<?> handleFileNotFoundException(FileNotFoundException fileNotFoundException) {
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body("¡Se ha producido un error para encontrar archivo JSON Schema!");
	}

	@ExceptionHandler(JsonMappingException.class)
    public ResponseEntity<?> handleJsonMappingException(JsonMappingException jsonMappingException) {
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body("¡Se ha producido un error al transformar ResponseDesafio (String) en JsonNode!");
	}
	
	@ExceptionHandler(DatabindException.class)
    public ResponseEntity<?> handleDatabindException(DatabindException databindException) {
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body("¡Se ha producido un error para leer archivo JSON Schema!");
	}

	@ExceptionHandler(JsonProcessingException.class)
    public ResponseEntity<?> handleJsonProcessingException(JsonProcessingException jsonProcessingException) {
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body("¡Se ha producido un error al transformar ResponseDesafio a JSON String!");
	}
}
