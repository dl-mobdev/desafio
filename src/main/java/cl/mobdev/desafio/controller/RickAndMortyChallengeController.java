package cl.mobdev.desafio.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;

import cl.mobdev.desafio.interf.IRickAndMortyChallenge;
import cl.mobdev.desafio.model.ResponseDesafio;
import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;


@RestController
@RequestMapping("desafio-mobdev")
public class RickAndMortyChallengeController {
	private static final Logger log = LoggerFactory.getLogger(RickAndMortyChallengeController.class);
	
	@Autowired
	private IRickAndMortyChallenge rickAndMortyChallengeImpl;
	
	@GetMapping("/personaje/{id}")
	public ResponseEntity<?> getResponseDesafio(
			@PathVariable Integer id) throws StreamReadException, DatabindException, FileNotFoundException, IOException {
		RMCharacter personaje = rickAndMortyChallengeImpl.getCharacterFromServiceById(id);
		Integer idOriginLocation = rickAndMortyChallengeImpl.getIdOriginLocationFromCharacter(personaje);
		RMLocation localizacion= rickAndMortyChallengeImpl.getLocationFromServiceById(idOriginLocation);
		
		ResponseDesafio responseDesafio = rickAndMortyChallengeImpl.mergeCharacterOriginLocation(personaje, localizacion);
		

		List<String> validationMessages = rickAndMortyChallengeImpl.validateResponseWhithJsonSchema(responseDesafio);
		
		if (!validationMessages.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(Map.of("Error en validación de schema", validationMessages));
		}
		
		if (responseDesafio == null) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(responseDesafio);
		}
	}
}
