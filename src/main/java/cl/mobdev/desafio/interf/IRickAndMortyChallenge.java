package cl.mobdev.desafio.interf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;

import cl.mobdev.desafio.model.ResponseDesafio;
import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;

public interface IRickAndMortyChallenge {

	/**
	 * Obtiene personaje por id desde servicio https://rickandmortyapi.com/
	 * @param id
	 * @return DTO contiene información del personaje.
	 */
	RMCharacter getCharacterFromServiceById(Integer id);
	
	/**
	 * Obtiene id localización origen desde DTO RMCharacter.
	 * @param rmCharacter 
	 * @return URL de localización.
	 */
	Integer getIdOriginLocationFromCharacter(RMCharacter rmCharacter);
	
	/**
	 * Obtiene localización por id desde servicio https://rickandmortyapi.com/
	 * @param id
	 * @return DTO contiene información de localización.
	 */
	RMLocation getLocationFromServiceById(Integer id);
	
	/**
	 * Mezcla información del personaje con localización de origen.
	 * @return DTO resultado del desafío
	 */
	ResponseDesafio mergeCharacterOriginLocation(RMCharacter rmCharacter, RMLocation rmLocation);

	/**
	 * Valida objeto resultado con JSON Schema.
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws DatabindException
	 * @throws StreamReadException
	 * @return Listado de String con mensajes de validación.
	 */
	List<String> validateResponseWhithJsonSchema(ResponseDesafio responseDesafio) throws FileNotFoundException, StreamReadException, DatabindException, IOException;


}
