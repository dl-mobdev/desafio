package cl.mobdev.desafio.repo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;

@FeignClient(value="ramac", url="https://rickandmortyapi.com/api")
public interface RickAndMortyApiClient {

	@RequestMapping(method=RequestMethod.GET, value="/character/{id}")
	RMCharacter getSingleCharacter(@PathVariable("id") Integer id);
	
	@RequestMapping(method=RequestMethod.GET, value="/location/{id}")
	RMLocation getSingleLocation(@PathVariable("id") Integer id);
}
