package cl.mobdev.desafio.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDesafio {
	Integer id;
	String name;
	String status;
	String species;
	String type;
	@JsonProperty("episode_count")
	Integer episodeCount;
	Origin origin;
	
	public class Origin {
		String name;
		String url;
		String dimension;
		List<String> residents;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getDimension() {
			return dimension;
		}
		public void setDimension(String dimension) {
			this.dimension = dimension;
		}
		public List<String> getResidents() {
			return residents;
		}
		public void setResidents(List<String> residents) {
			this.residents = residents;
		}
		
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getEpisodeCount() {
		return episodeCount;
	}

	public void setEpisodeCount(Integer episodeCount) {
		this.episodeCount = episodeCount;
	}

	public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin;
	}
	
	
}
