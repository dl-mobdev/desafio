package cl.mobdev.desafio.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.mobdev.desafio.model.ResponseDesafio;
import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;
import feign.FeignException;

/**
 * Tests desarrollados para evaluar proceso de:
 * 
 * - obtención de data desde https://rickandmortyapi.com/
 * - procesamiento de información
 * - generación de objeto esperado como resultado
 * 
 * Se ejecutan en un orden específico. Son dependientes., para ello se utiliza 
 * la anotación @Order.
 *  
 * @author mauricio
 *
 */
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RickAndMortyChallengeImplTest {
	
	private static final Logger log = LoggerFactory.getLogger(RickAndMortyChallengeImplTest.class);

	@Autowired
	private RickAndMortyChallengeImpl rickAndMortyChallengeImpl;
	
	@Autowired
	private ObjectMapper om;
	
	private RMCharacter personaje;
	private Integer idLocalizacionOrigen;
	private RMLocation localizacion;
	private ResponseDesafio responseDesafio;
	
	@Test
	@Order(10)
	void getCharacterFromServiceByIdTest() {
		Integer idPersonaje = 1;
		try {
			personaje = rickAndMortyChallengeImpl.getCharacterFromServiceById(idPersonaje);
		}
		catch (FeignException fe) {
			log.error("Error de conexión con api https://rickandmortyapi.com: {}", fe.getMessage());
		}
		assertNotNull(personaje);
	}
	
	@Test
	@Order(30)
	void getLocationFromCharacterTest() {
		idLocalizacionOrigen = rickAndMortyChallengeImpl.getIdOriginLocationFromCharacter(personaje);
		
		assertTrue(idLocalizacionOrigen > 0);
	}

	@Test
	@Order(50)
	void getLocationFromServiceByIdTest() {
		try {
			localizacion = rickAndMortyChallengeImpl.getLocationFromServiceById(idLocalizacionOrigen);
		}
		catch (FeignException fe) {
			log.error("Error de conexión con api https://rickandmortyapi.com: {}", fe.getMessage());
		}
		
		assertNotNull(localizacion);
	}
	
	@Test
	@Order(70)
	void mergeCharacterOriginLocationTest() {
		responseDesafio = rickAndMortyChallengeImpl.mergeCharacterOriginLocation(personaje, localizacion);
		
		assertNotNull(responseDesafio);
	}
	
	@Test
	@Order(90)
	void validateResponseWhitJsonSchemaTest_OK() throws IOException, JSONException {

		List<String> mensajes = rickAndMortyChallengeImpl.validateResponseWhithJsonSchema(responseDesafio);
		assertNotNull(mensajes);
		assertTrue(mensajes.isEmpty());
	}
}
