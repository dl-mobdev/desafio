package cl.mobdev.desafio.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.mobdev.desafio.interf.IRickAndMortyChallenge;
import cl.mobdev.desafio.model.ResponseDesafio;
import cl.mobdev.desafio.model.rickandmortyapi.RMCharacter;
import cl.mobdev.desafio.model.rickandmortyapi.RMLocation;

/**
 * Tests utilizados para validar funcionalidades específicas de la interfaz IRickAndMortyChallenge. 
 * No importa el orden en que se ejecuten. Son tests independientes.
 * @author mauricio
 *
 */
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class RickAndMortyChallengeImplUnitTest {
	private static final Logger log = LoggerFactory.getLogger(RickAndMortyChallengeImplUnitTest.class);

	@Autowired
	private IRickAndMortyChallenge rickAndMortyChallengeImpl;
	
	@Autowired
	private ObjectMapper om;
	
	private RMCharacter personaje;
	private RMLocation localizacion;

    @BeforeAll
    void setup() {
    	personaje = this.getPersonajeFromJson();
		localizacion = this.getLocalizacionFromJson();
    }
    
    private RMCharacter getPersonajeFromJson() {
    	String resourceLocation = "classpath:character-example.json";

    	RMCharacter personaje = null;
    	try {
			URL urlFileJson = ResourceUtils.getURL(resourceLocation);
			personaje = om.readValue(urlFileJson, RMCharacter.class);

		} catch (IOException eio) {
			// TODO Auto-generated catch block
			eio.printStackTrace();
		}
    	
    	return personaje;
    }
    
    private RMLocation getLocalizacionFromJson() {
    	String resourceLocation = "classpath:location-example.json";
    	RMLocation localizacionFromJson = null;
    	try {
	    	URL fileJson = ResourceUtils.getURL(resourceLocation);
	    	localizacionFromJson = om.readValue(fileJson, RMLocation.class);
		} catch (IOException eio) {
			// TODO Auto-generated catch block
			eio.printStackTrace();
		}
		
		return localizacionFromJson;
	}
	
	private ResponseDesafio getResponseDesafioFromJson() {
		String resourceLocation = "classpath:response-desafio-example.json";
		ResponseDesafio responseDesafioFromJson = null;
		try {
			URL fileJson = ResourceUtils.getURL(resourceLocation);
			responseDesafioFromJson = om.readValue(fileJson, ResponseDesafio.class);
		} catch (FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
		} catch (IOException eio) {
			// TODO Auto-generated catch block
			eio.printStackTrace();
		}
    	
		return responseDesafioFromJson;
    }
	@Test
	void mergeCharacterOriginLocationTest() {
		ResponseDesafio response = rickAndMortyChallengeImpl.mergeCharacterOriginLocation(personaje, localizacion);
		assertNotNull(response);
		
		try {
			String responseString = om.writeValueAsString(response);
			log.debug("responseString: {}", responseString);
		}
		catch (JsonProcessingException jpe) {
			log.error("Error procesando respuesta como JSON");
		}
	}
	
	@Test
	void validateResponseWhithJsonSchemaTest_OK() throws IOException, JSONException {
		ResponseDesafio responseDesafioExample = this.getResponseDesafioFromJson();
		List<String> mensajes = rickAndMortyChallengeImpl.validateResponseWhithJsonSchema(responseDesafioExample);
		assertNotNull(mensajes);
		assertTrue(mensajes.isEmpty());
	}
	
	@Test
	void validateResponseWhithJsonSchemaTest_NOk() {
		ResponseDesafio responseDesafioExample = this.getResponseDesafioFromJson();
		responseDesafioExample.setSpecies(null);
		
		List<String> mensajes;
		try {
			mensajes = rickAndMortyChallengeImpl.validateResponseWhithJsonSchema(responseDesafioExample);
			
			assertNotNull(mensajes);
			assertFalse(mensajes.isEmpty());
			assertTrue(mensajes.get(0).contains("$.species: null found"));
		}
		catch (FileNotFoundException fnf) {
			log.error("Se ha producido un error para encontrar archivo JSON Schema: {}", fnf.getMessage());
		}
		catch(JsonMappingException jme) {
			log.error("Se ha producido un error al transformar ResponseDesafio (String) en JsonNode: {}", jme.getMessage());
		}
		catch (DatabindException dbe) {
			log.error("Se ha producido un error para leer archivo JSON Schema: {}", dbe.getMessage());
		}
		catch(JsonProcessingException jpe) {
			log.error("Se ha producido un error al transformar ResponseDesafio en JSON String: {}", jpe.getMessage());
		}
		catch(IOException ioe) {
			log.error("Se ha producido un error: {}", ioe.getMessage());
		}
	}
}
