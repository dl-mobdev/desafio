# Desafío Técnico Mobdev #

El presente proyecto ha sido desarrollado en el marco de una prueba técnica para postular al cargo de 
desarrollador en MobDev.

## Desafío ##

El desafío consiste en usar la API de Rick And Morty

* https://rickandmortyapi.com/documentation/#get-a-single-character
* https://rickandmortyapi.com/documentation/#get-a-single-location

Exponer un endpoint que permita realizar una petición con el id de algún personaje de la serie, se debe
entregar una respuesta esperada por el equipo MobDev.

Ver [documento de requerimientos](src/main/resources/Rick And Morty Challenge.pdf)


## Diseño de solución y testing ##

Como primer elemento de diseño se crea la interfaz IRickAndMortyChallenge:

![interfaz](src/main/resources/interfaz.png)

Esta interfaz contiene la declaración de los métodos que se deberán implementar:

	/**
	 * Obtiene personaje por id desde servicio https://rickandmortyapi.com/
	 */
	RMCharacter getCharacterFromServiceById(Integer id);

	/**
	 * Obtiene id localización origen desde DTO RMCharacter.
	 */
	Integer getIdOriginLocationFromCharacter(RMCharacter rmCharacter);
	
	/**
	 * Obtiene localización por id desde servicio https://rickandmortyapi.com/
	 */
	RMLocation getLocationFromServiceById(Integer id);

	/**
	 * Mezcla información del personaje con localización de origen.
	 */
	ResponseDesafio mergeCharacterOriginLocation(RMCharacter rmCharacter, RMLocation rmLocation);

	/**
	 * Valida objeto resultado con JSON Schema.
	 */
	List<String> validateResponseWhithJsonSchema(ResponseDesafio responseDesafio) throws FileNotFoundException, StreamReadException, DatabindException, IOException;


Luego, se implementa la interfaz en la clase RickAndMortyChallengeImpl:

![implementacion](src/main/resources/implementacion.png)

En esta etapa del proceso, se desarrollan tests que ayudan a su vez a desarrollar los componentes de la implementación. 


## Diagrama de solución ##

![diagrama](src/main/resources/diagrama-desafio.png)


### RickAndMortyChallengeImplUnitTest ###

 Tests utilizados para validar funcionalidades específicas de la interfaz IRickAndMortyChallenge. 
 No importa el orden en que se ejecuten. Son tests independientes.

[src/test/java/cl/mobdev/desafio/service/RickAndMortyChallengeImplUnitTest.java](src/test/java/cl/mobdev/desafio/service/RickAndMortyChallengeImplUnitTest.java)

### RickAndMortyChallengeImplTest ###

Tests desarrollados para evaluar proceso de:

* obtención de data desde https://rickandmortyapi.com/
* procesamiento de información
* generación de objeto esperado como resultado

Se ejecutan en un orden específico. Son dependientes.

[src/test/java/cl/mobdev/desafio/service/RickAndMortyChallengeImplTest.java](src/test/java/cl/mobdev/desafio/service/RickAndMortyChallengeImplTest.java)


## Implementación de endpoint ##

Finalmente, se expone el endpoint que permite realizar una petición con el id de algún personaje de la serie. 

Para efectos de este desafío técnico se realizan pruebas utilizando cliente Postman. La colección de requests Postman utilizados en esta prueba puede ser encontrada en archivo [desafio_tecnico.postman_collection.json](src/main/resources/desafio_tecnico.postman_collection.json).

### Prueba exitosa: datos del personaje y localización ###

![implementacion](src/main/resources/postman-200.png)

### Prueba fallida: personaje no encontrado ###

![implementacion](src/main/resources/postman-404.png)

#### Prueba fallida: id no numérico ####

![implementacion](src/main/resources/postman-409.png)


